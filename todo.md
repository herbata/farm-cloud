- [x] Rename Transaction to something fucking else
- [x] User roles / lock down the admin
      Rolify / Pundit or CanCanCan
- [x] US Subscription Product (find correct products)
- [ ] New photos index + logo
- [ ] Fix after sign in path devise
- [ ] Responsive things
- [ ] Geocoder Timeout Issue




# To Do


- [ ] Create Plans
- [ ] Create Subscriptions
- [ ] Create Cards
- [ ] Create Coupons
- [ ] Create Discounts
- [ ] Create Taxes
- [ ] Integrate Stripe (add customer token to customer)
- [ ] Add Payment Options
- [ ] CRUD Actions on: Plans, Subscriptions, Coupons, Discounts, Products, ProductGroups, Taxes, Locations,
                       Customers, Users, Properties, Product Variants, Orders
- [ ] Versioned API for products / users / customers / orders / subscriptions



rails g model ProductType name:string description:text
rails g model ProductProductType product:references product_type:references

## Product

```ruby
  name: "Platinum starter" # Displayed on Invoices / Web Interface
  description: "" ## Optional for internal use
  active: Boolean ## Ability for us internally to hide to public
  price: Integer # in cents
  currency: "CAD" || "USD"
  product_group_id: ProductGroup.id

  ## Subscription

HERBATA_TEA_MONTHLY
HERBATA_TEA_3M_GIFT

  stripe_id: "string"
  interval: "day" || "month" || "week" || "year"
  interval_count: Integer # intervals between charges

  ## Optional Below
  metadata: "" # Object stored in stripe for whatever
  statement_descriptor: "" # Appears on CC bills, Max 22 Char, can't include <>"'
  trial_period_days: Integer # In Days
```

rails g scaffold product name:string description:text active:boolean price:integer currency:string stripe_id:string interval:string interval_count:Integer statement_descriptor:string trial_period_days:Integer product_group:references


## Subscription

```ruby
  customer_id: Customer.id
  product_id: Product.id
  coupon_if: Coupon.id
  uuid: String
  stripe_subscription_id: String
  stripe_customer_id: String
  stripe_coupon_id: String
  stripe_plan_id: String
  quantity: Integer
  tax_percent: Float
  trial_end: UnixTimestamp
  canceled_at: Datetime
```

rails g scaffold subscription customer:references product:references coupon:references uuid:string stripe_subscription_id:string stripe_customer_id:string stripe_coupon_id:string stripe_plan_id:string quantity:integer tax_percent:float trial_end:string canceled_at:datetime

## Transactions

```ruby

  customer_id: Customer
  order: Order
  subscription: Subscription

  stripe_status:
  stripe_customer:
  stripe_order:


```


## Coupons

```ruby
  name: "Internal Use"
  code: "20OFF" # discount code
  duration: "" # forever, once, or repeating
  duration_in_months: Integer # # of Months
  amount_off: Integer
  percent_off: Integer # 1-100
  currency: String
  max_redemptions: Integer
  redeem_by: Datetime
```

rails g scaffold coupons name:string code:string duration:string duration_in_months:integer amount_off:integer percent_off:integer currency:string max_redemptions:integer reedem_by:datetime

## Discount

```ruby
  coupon_id: Coupon.id
  customer_id: Customer.id
  subscription_id: Subscription.id
  start: DateTime
  end: DateTime
```

rails g model discount coupon:references customer:references subscription:references start:datetime end:datetime

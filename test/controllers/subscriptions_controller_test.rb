require 'test_helper'

class SubscriptionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @subscription = subscriptions(:one)
  end

  test "should get index" do
    get subscriptions_url
    assert_response :success
  end

  test "should get new" do
    get new_subscription_url
    assert_response :success
  end

  test "should create subscription" do
    assert_difference('Subscription.count') do
      post subscriptions_url, params: { subscription: { canceled_at: @subscription.canceled_at, coupon_id: @subscription.coupon_id, customer_id: @subscription.customer_id, product_id: @subscription.product_id, quantity: @subscription.quantity, stripe_coupon_id: @subscription.stripe_coupon_id, stripe_customer_id: @subscription.stripe_customer_id, stripe_plan_id: @subscription.stripe_plan_id, stripe_subscription_id: @subscription.stripe_subscription_id, tax_percent: @subscription.tax_percent, trial_end: @subscription.trial_end, uuid: @subscription.uuid } }
    end

    assert_redirected_to subscription_url(Subscription.last)
  end

  test "should show subscription" do
    get subscription_url(@subscription)
    assert_response :success
  end

  test "should get edit" do
    get edit_subscription_url(@subscription)
    assert_response :success
  end

  test "should update subscription" do
    patch subscription_url(@subscription), params: { subscription: { canceled_at: @subscription.canceled_at, coupon_id: @subscription.coupon_id, customer_id: @subscription.customer_id, product_id: @subscription.product_id, quantity: @subscription.quantity, stripe_coupon_id: @subscription.stripe_coupon_id, stripe_customer_id: @subscription.stripe_customer_id, stripe_plan_id: @subscription.stripe_plan_id, stripe_subscription_id: @subscription.stripe_subscription_id, tax_percent: @subscription.tax_percent, trial_end: @subscription.trial_end, uuid: @subscription.uuid } }
    assert_redirected_to subscription_url(@subscription)
  end

  test "should destroy subscription" do
    assert_difference('Subscription.count', -1) do
      delete subscription_url(@subscription)
    end

    assert_redirected_to subscriptions_url
  end
end

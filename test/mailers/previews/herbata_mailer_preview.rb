# Preview all emails at http://localhost:3000/rails/mailers/herbata_mailer
class HerbataMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/herbata_mailer/new_order
  def new_order
    HerbataMailer.new_order
  end

end

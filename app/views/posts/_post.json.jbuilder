json.extract! post, :id, :title, :published, :user_id, :meta_image, :cover_image, :content, :created_at, :updated_at
json.url post_url(post, format: :json)
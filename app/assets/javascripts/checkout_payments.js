$(document).on('turbolinks:load', function() {
  $(function() {
    var $form = $('.js-payment-form');
    $form.submit(function(event) {
      // Disable the submit button to prevent repeated clicks:
      $form.find('.submit').prop('disabled', true);

      // Request a token from Stripe:
      Stripe.card.createToken($form, stripeResponseHandler);

      // Prevent the form from being submitted:
      return false;
    });
  });

  function stripeResponseHandler(status, response) {
    var $form = $('.js-payment-form');

    if (response.error) {
      $form.find('.payment-errors').text(response.error.message);
      $form.find('.submit').prop('disabled', false); // Re-enable submission
    } else {
      $form.append($('<input type="hidden" name="[payment]stripe_token">').val(response.id));
      $form.append($('<input type="hidden" name="[payment]stripe_last_four">').val(response.card.last4));
      $form.append($('<input type="hidden" name="[payment]stripe_brand">').val(response.card.brand));
      $form.append($('<input type="hidden" name="[payment]stripe_exp_month">').val(response.card.exp_month));
      $form.append($('<input type="hidden" name="[payment]stripe_exp_year">').val(response.card.exp_year));
      $form.append($('<input type="hidden" name="[payment]stripe_name">').val(response.card.name));
      $form.append($('<input type="hidden" name="[payment]stripe_id">').val(response.card.id));

      $form.get(0).submit();
    }
  };
});
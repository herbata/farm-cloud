//= require jquery
//= require tether
//= require jquery_ujs
//= require turbolinks


$(document).on('turbolinks:load', function() {

  // Mobile Navigation

    $('.js-nav-trigger').on('click', function() {
      $('.js-nav-items').toggleClass('is-visible');
    });



  // Smooth Scrolling

    $(function() {
      $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html, body').animate({
              scrollTop: target.offset().top
            }, 1000);
            return false;
          }
        }
      });
    });

});
class PasswordsController < Devise::PasswordsController
  layout 'frontend'
  skip_load_and_authorize_resource
end

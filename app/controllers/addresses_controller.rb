class AddressesController < ApplicationController
  layout :account
  before_action :set_address, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  def index
    @addresses = current_user.addresses
  end
  
  def show
  end

  def new
    @address = Address.new
  end
  
  def create
    if current_customer
      @address = current_customer.addresses.new(address_params)

      if @address.save
        if params[:continue_checkout]
          current_order.shipping_address_id = @address.id
          current_order.save!
          redirect_to checkout_payment_path
        else
          redirect_to addresses_path, notice: "Address saved!"
        end
      else
        redirect_to :back
      end
    end
  end
  
  def edit
  end

  def update
    if @address.update(address_params)
      redirect_to addresses_path, notice: "It's been updated"
    else
      render :edit
    end
  end 

  protected

  def set_address
    @address = Address.find(params[:id])
  end

  def address_params
    params.require(:address).permit(
      :first_name,
      :last_name,
      :line_one,
      :line_two,
      :line_three,
      :city,
      :province,
      :country,
      :postal_code
    )
  end

end

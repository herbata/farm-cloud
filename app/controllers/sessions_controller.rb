class SessionsController < Devise::SessionsController
  layout 'frontend'
  skip_load_and_authorize_resource
end

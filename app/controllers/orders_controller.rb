class OrdersController < ApplicationController
  layout "application"
  before_action :authenticate_user!

  before_action :find_order, only: [:show, :edit, :update]

  def index
    @orders = Order.placed
  end

  def show
  end

  def edit
  end

  def update
    if @order.save
      redirect_to @order, notice: "Order Successfully Updated"
    else
      render :edit, notice: "Uhoh, something didn't work"
    end
  end

  private

  def find_order
    @order = Order.find(params[:id])
    @order_statuses = @order.order_statuses
  end

  def order_params
    params.require(:order).permit(:notes)
  end
end

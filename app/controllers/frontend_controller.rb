class FrontendController < ApplicationController
  skip_load_and_authorize_resource
  
  before_action :generate_welcome_text
  before_action :set_products, only: [:index, :products]

  def index
    @order_item = current_order.order_items.new

  end

  def cart
    @order_items = current_order.order_items
  end

  def about
  end

  def blog
  end

  def products
    @order_item = current_order.order_items.new
  end

  def order
  end

  def contact
  end

  def checkout_account
  end

  def checkout_shipping
    @addresses = current_customer.addresses
    @address = current_customer.addresses.new
  end

  def checkout_payment
    @order = current_order
end

  def checkout_confirmation
  end

  def checkout_payment_fulfillment
    begin
      @order = current_order
      @customer = current_customer
      @user = current_user

      if @order.active?
        if !@customer.user
          @user.link_customer(@customer)
        end

        if payment_params[:stripe_token]
          if @customer.generate_stripe_customer_from_token(payment_params[:stripe_token])

            @customer.cards.create!({
              name: payment_params[:stripe_name],
              last_four: payment_params[:stripe_last_four],
              stripe_id: payment_params[:stripe_id],
              stripe_customer_id: @customer.stripe_id,
              brand: payment_params[:stripe_brand],
              exp_month: payment_params[:stripe_exp_month],
              exp_year: payment_params[:stripe_exp_year],
            })

            if @order.customer != @customer
              @order.customer = @customer
            end

            if @order.place
              session[:order_id] = nil
              redirect_to :checkout_confirmation, notice: "Heart Followed...Check! Everything worked out."
            end
          else
            puts "----------------"
            puts "----------------"
            puts "WE COULDNT GENERATED A STRIPE CUSTOMER. GROSS. FAILURE."
            puts "----------------"
            puts "----------------"
          end
        end
      else
        puts "----------------"
        puts "----------------"
        puts "FAILURE. THEY BROKE EVERYTHING. THIS ORDER ISN'T ACTIVE.. WHAT THE F...?"
        puts "----------------"
        puts "----------------"
      end

    rescue => e
      @order.errors.add(:card, e.message)
      render :checkout_payment, notice: 'Uh Oh'
    end
  end

  def letsencrypt_develop
    render text: "krDZFT9l6mV5mh29OuTAinc3-uCDZudYljSon6gH41I.yjTipuicGfT-LSupVIWyQNwTsjmJdMVWaZHaN2rFfN4"
  end

  def letsencrypt_staging
    render text: "bZQzDR1ujn0FQ0UuIJYwrpHINu-Jkpnv4I1XtAvcNhs.yjTipuicGfT-LSupVIWyQNwTsjmJdMVWaZHaN2rFfN4"
  end

  def letsencrypt_production
    render text: "4ahKSK8Se_NIIpBnfipB0xIFNU6mhg69pocJU4GzV8o.yjTipuicGfT-LSupVIWyQNwTsjmJdMVWaZHaN2rFfN4"
  end

  private

  def set_products
    currency = (@user_location === "Canada" ? "CAD" : "USD")
    @products = ProductGroup.find(1).products.where(currency: currency)
  end    

  def generate_welcome_text
    @welcome_text = user_signed_in? ? "WELCOME, " + current_user.name.upcase : "ACCOUNT"
  end

  def payment_params
    params.require(:payment).permit(
      :stripe_token,
      :stripe_id,
      :stripe_brand,
      :stripe_last_four,
      :stripe_name,
      :stripe_exp_month,
      :stripe_exp_year
    )
  end
end

class DashboardController < ApplicationController
  layout "application"
  before_action :authenticate_user!
  before_action :auth_user
  skip_load_and_authorize_resource
  
  def index
    # @orders = Order.all
    # @customers = Customer.all
  end

  private

  def auth_user
    unless current_user.has_role? "admin"
      redirect_to account_path
    end
  end
end

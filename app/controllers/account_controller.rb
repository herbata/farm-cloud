class AccountController < FrontendController
  layout 'account'
  
  before_action :authenticate_user!
  skip_load_and_authorize_resource

  def index
    @orders = []

    if current_user.customers
      current_user.customers.each do |c|
        c.previous_and_placed_orders.each do |o|
          @orders << o
        end
      end
    end
  end

  def subscriptions
    @subscriptions = []

    if current_user.customers
      current_user.customers.each do |c|
        c.subscriptions.each do |s|
          @subscriptions << s
        end
      end
    end
  end

  def billing
  end

  def settings
    @user = current_user
  end

  def order
  end
end

require 'net/http'
require 'json'

class ApplicationController < ActionController::Base
  layout "frontend"
  protect_from_forgery with: :exception

  load_and_authorize_resource

  before_action :locate_user
  helper_method :current_customer
  helper_method :current_order

  def locate_user
     session[:user_location] = lookup_ip(request.remote_ip) unless session[:user_location]
     @user_location = session[:user_location]
  end

  def lookup_ip(ip)
    uri = URI("http://api.wipmania.com/" + ip + "?herbata.co")
    response = Net::HTTP.get(uri)
    
    return response === "CA" ? "Canada" : "Other"
  end


  def current_customer
    customer = nil
    
    ## Is there a user?
    if user_signed_in?

      # Do they have a customer already?
      if current_user.customers.size > 0
        customer = current_user.customers.first
      
      # No? Is there a customer in the cookies?
      elsif session[:customer_id] && Customer.exists?(session[:customer_id])
        customer = Customer.find(session[:customer_id])
        current_user.link_customer(customer)
      
      # Oh well, make a new customer for them
      else
        customer = current_user.create_customer
      end
    
    # No User, but is there a customer in the cookies?
    elsif session[:customer_id] && Customer.exists?(session[:customer_id])
      customer = Customer.find(session[:customer_id])

    # Nothing at all? Let's make a user and add it to the browser
    else
      customer = Customer.create
      session[:customer_id] = customer.id
    end

    customer
  end

  def current_order
    if session[:order_id] && Order.exists?(session[:order_id])
      order = Order.find(session[:order_id])
      
      order = Order.new unless order.active?
    else
      order = Order.new
    end
    order.currency_code = (@user_location === "Canada" ? "CAD" : "USD") if order.currency_code.blank?

    return order
  end
end

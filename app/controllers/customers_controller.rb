class CustomersController < ApplicationController
  layout "application"
  before_action :authenticate_user!
  before_action :set_customer, only: [:show, :edit, :update, :destroy]

  # GET /customers
  # GET /customers.json
  def index
    @customers = Customer.all
  end

  # GET /customers/1
  # GET /customers/1.json
  def show
  end

  # GET /customers/new
  def new
    @customers = Customer.new
  end

  # GET /customers/1/edit
  def edit
  end

  # POST /customers
  # POST /customers.json
  def create
    @customers = Customer.new(customer_params)

    respond_to do |format|
      if @customers.save
        format.html { redirect_to @customers, notice: 'Customer was successfully created.' }
        format.json { render :show, status: :created, location: @customers }
      else
        format.html { render :new }
        format.json { render json: @customers.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /customers/1
  # PATCH/PUT /customers/1.json
  def update
    respond_to do |format|
      if @customers.update(customer_params)
        format.html { redirect_to @customers, notice: 'Customer was successfully updated.' }
        format.json { render :show, status: :ok, location: @customers }
      else
        format.html { render :edit }
        format.json { render json: @customers.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /customers/1
  # DELETE /customers/1.json
  def destroy
    @customers.destroy
    respond_to do |format|
      format.html { redirect_to customers_url, notice: 'Customer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_customer
      @customers = Customer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def customer_params
      params.require(:Customer).permit(:name, :code, :duration, :duration_in_months, :amount_off, :percent_off, :currency, :max_redemptions, :reedem_by)
    end
end

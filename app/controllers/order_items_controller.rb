class OrderItemsController < ApplicationController

  def create
    @order = current_order
    @order.customer = current_customer

    @product = Product.find(params["product_id"])
    
    @order.order_items.new({
      quantity: order_item_params["quantity"],
      fee_total: 0,
      sub_total: @product.price,
      total: @product.price,
      product: @product
    })
    
    if @order.save!
      puts "----------"
      puts "----------"
      @order.id
      puts "----------"
      session[:order_id] = @order.id
      puts session[:order_id]
      puts "----------"
      puts "----------"
      redirect_to cart_path
    end
  end

  def update
    @order = current_order
    @order_item = @order.order_items.find(params[:id])
    @order_item.update_attributes(order_item_params)
    @order_items = @order.order_items
  end

  def destroy
    @order = current_order

    if @order.active?
      @order_item = @order.order_items.find(params[:id])
      
      if @order_item.destroy
        redirect_to cart_path, notice: "Poof! Item has been removed from your cart"
      else
        redirect_to cart_path
      end
    else
      redirect_to cart_path, notice: "Sneaky, this order has already been placed"
    end
  end
  private

  def order_item_params
    params.require(:order_item).permit(:quantity, :product_id)
  end


end

class ApplicationMailer < ActionMailer::Base
  default from: 'yourfriends@herbata.co'
  layout 'mailer'
end

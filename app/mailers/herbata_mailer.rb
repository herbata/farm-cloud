class HerbataMailer < ApplicationMailer

  def new_order(order)
    @order = order
    @customer = order.customer
    @user = @customer.user
    @address = Address.find(@order.billing_address_id)

    mail to: "jeremy@herbata.co"
  end
end

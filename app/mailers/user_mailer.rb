class UserMailer < ApplicationMailer

  def welcome(user)
    @user = user
    @url = "https://herbata.co/account"

    mail(
      :to => @user.email,
      :subject => "Welcome to Herbata"
    )
  end
end

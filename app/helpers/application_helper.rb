module ApplicationHelper

  def menu_link(title, path, options = nil)
    if current_page?(path)
      options = options || {}
      options[:class] = 'is-active'
    end

    return content_tag(:li, link_to(title, path, options))
  end

  def empty_cart?
    current_order.order_items.count < 1
  end

  def checkout_flow?
    current_page?(cart_path) ||
    current_page?(checkout_account_path) ||
    current_page?(checkout_shipping_path) ||
    current_page?(checkout_payment_path) ||
    current_page?(checkout_confirmation_path)
  end

  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def current_name
    user_signed_in? ? current_user.first_name : "Human"
  end

  def order_total_with_currency(order)
    html = "$" + (order.total.to_i / 100).to_s + " <small>" + order.currency_code + "</small>"
    html.html_safe
  end

  def subscription_total_with_currency(subscription)
    html = "$" + (subscription.product.price.to_i / 100).to_s + " <small>" + subscription.product.currency_code + "</small>"
    html.html_safe
  end

end

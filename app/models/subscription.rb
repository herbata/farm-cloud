class Subscription < ApplicationRecord
  belongs_to :customer
  belongs_to :product
  belongs_to :coupon

  def self.create_subscription(product, customer)
    if product.product_type.id === 2
      stripe_subscription = Stripe::Subscription.create(
        :customer => customer.stripe_id,
        :plan => product.stripe_id
      )

      Subscription.create!({
        uuid: SecureRandom.hex,
        customer: customer,
        product: product,
        stripe_id: stripe_subscription[:id],
        stripe_customer_id: stripe_subscription[:customer],
        stripe_plan_id: stripe_subscription[:plan][:id],
        quantity: stripe_subscription[:quantity],
        tax_percent: stripe_subscription[:tax_percent],
        trial_end: stripe_subscription[:trial_end],
        canceled_at: stripe_subscription[:canceled_at]
      })

    end
  end

  def cancel
    stripe_subscription = Stripe::Subscription.retrieve(self.stripe_id)
    res = stripe_subscription.delete

    self.active = false if res[:canceled] || res[:cancel_at_period_end]
    self.canceled_at = Time.now
    self.save!
  end
end

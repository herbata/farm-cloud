class Discount < ApplicationRecord
  belongs_to :coupon
  belongs_to :customer
  belongs_to :subscription
end

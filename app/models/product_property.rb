class ProductProperty < ApplicationRecord
  belongs_to :product
  belongs_to :property

  validates :name,  presence: true
  validates :price, presence: true
end

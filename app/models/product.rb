class Product < ApplicationRecord
  belongs_to :product_group
  has_many :product_properties
  
  has_one :product_product_type
  has_one :product_type, through: :product_product_type

  validates :name,  presence: true
  validates :price, presence: true

  after_create :generate_stripe_plan

  def currency_code
    self.currency
  end
  

  # private

  def generate_stripe_plan
    if self.interval && self.interval_count && self.stripe_id
      begin Stripe::Plan.retrieve(self[:stripe_id])
      
      rescue Stripe::InvalidRequestError
        Stripe::Plan.create(
          :name => self[:name],
          :id => self[:stripe_id],
          :interval => self[:interval],
          :interval_count => self[:interval_count],
          :trial_period_days => self[:trial_period_days],
          :statement_descriptor => self[:statement_descriptor],
          :amount => self[:price],
          :currency => self[:currency]
        )
      end
    end
  end
end

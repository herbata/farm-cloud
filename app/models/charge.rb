class Charge < ApplicationRecord
  def self.create_charge(customer, amount, currency, description = 'Purchase from Herbata Tea Co.')
    charge = Stripe::Charge.create(
      :amount => amount.to_i,
      :currency => currency,
      :description => description,
      :customer => customer.stripe_id,
    )

    Charge.create({
      stripe_id: charge["id"],
      stripe_customer_id: charge["customer"],
      # stripe_dispute_id: charge["dispute"],
      stripe_invoice_id: charge["invoice"],
      uuid: SecureRandom.hex,
      amount: charge["amount"],
      amount_refunded: charge["amount_refunded"],
      application: charge["application"],
      application_fee: charge["application_fee"],
      balance_transaction: charge["balance_transaction"],
      captured: charge["captured"],
      currency: charge["currency"],
      description: charge["description"],
      destination: charge["destination"],
      failure_code: charge["failure_code"],
      failure_message: charge["failure_message"],
      livemode: charge["livemode"],
      # outcome_type: charge["outcome"]["type"] || nil,
      # outcome_network_status: charge["outcome"]["network_status"] || nil,
      # outcome_reason: charge["outcome"]["reason"] || nil,
      # outcome_risk_level: charge["outcome"]["risk_level"] || nil,
      # outcome_rule_action: charge["outcome"]["rule"]["action"] || nil,
      # outcome_rule_predicate: charge["outcome"]["rule"]["predicate"] || nil,
      # outcome_seller_message: charge["outcome"]["seller_message"] || nil,
      # outcome_reason: charge["outcome"]["reason"] || nil,
      paid: charge["paid"],
      receipt_email: charge["receipt_email"],
      receipt_number: charge["receipt_number"],
      refunded: charge["refunded"],
      source: charge["source"],
      status: charge["status"],
    })
  end
end

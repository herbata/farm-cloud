class Customer < ApplicationRecord

  has_one :customer_user
  has_one :user, through: :customer_user

  has_many :address_customers
  has_many :addresses, through: :address_customers

  has_many :cards

  has_many :orders
  has_many :subscriptions

  def previous_and_placed_orders
    orders = []

    self.orders.each do |o|
      order_status = o.order_statuses.last
      if order_status && order_status.id > 1
        orders << o
      end
    end

    orders
  end
  
  def orders_by_status_name(status)
    orders = []

    self.orders.each do |o|
      order_status = o.order_statuses.last
      if order_status && order_status.name > 1
        orders << o
      end
    end

    orders
  end

  def orders_by_status_id(status)
    orders = []

    self.orders.each do |o|
      order_status = o.order_statuses.last
      if order_status && order_status.id > 1
        orders << o
      end
    end

    orders
  end

  def default_address
    if self.primary_address_id
      address = Address.find(self.primary_address_id)
    end

    address
  end

  def generate_stripe_customer_from_token(token)
    stripe_customer = Stripe::Customer.create(
      :email => self.user.email,
      :source => token
    )
    
    self.stripe_id = stripe_customer[:id]
    self.save!
  end
end

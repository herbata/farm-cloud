class Order < ApplicationRecord
  belongs_to :customer

  after_create :set_initial_order_status
  before_save :finalize

  has_many :order_statuses, through: :order_order_statuses
  has_many :order_order_statuses
  has_many :order_items, dependent: :destroy
  # has_one :location,
  #   dependent:  :destroy,
  #   as:         :owner,
  #   inverse_of: :owner

  # validates :sub_total,     presence: true
  # validates :fee_total,     presence: true
  # validates :total,         presence: true
  validates :currency_code, presence: true

  # accepts_nested_attributes_for :order_items
  # accepts_nested_attributes_for :location

  def self.placed
    orders = []
    Order.all.each do |o|
      orders << o if o.placed?
    end

    return orders
  end

  def status
    self.order_order_statuses.last
  end

  def active?
    if self.status.order_status_id.blank?
      false
    else
      self.status.order_status_id < 2
    end
  end

  def placed?
    if self.status.order_status_id.blank?
      false
    else
      self.status.order_status_id > 1
    end
  end

  def sub_total
    order_items.collect { |oi| oi.valid? ? oi.sub_total : 0 }.sum
  end

  def fee_total
    order_items.collect { |oi| oi.valid? ? oi.fee_total : 0 }.sum
  end 

  def place
    @customer = self.customer
    self.finalize

    total_charge_amount = 0
    product_names = []
    order_items.each do |oi|
      if oi.valid? && oi.product.product_type.id === 1
        oi.finalize
        product_names << oi.product.name
        total_charge_amount += oi.total
      elsif oi.valid? && oi.product.product_type.id === 2
        Subscription.create_subscription(oi.product, @customer)
      end
    end

    if total_charge_amount > 0
      Charge.create_charge(@customer, total_charge_amount, self.currency_code, "Charge for purchase of #{ product_names }")
    end

    if (@customer.previous_and_placed_orders.size === 0)
      UserMailer.welcome(@customer.user).deliver_now
    end

    HerbataMailer.new_order(self)

    self.bump_order_status
    return true
  end

  def shipping_address
    self.shipping_address_id ? Address.find(self.shipping_address_id) : OpenStruct.new({ country: "Not listed" })
  end

  protected

  def set_initial_order_status
    OrderOrderStatus.create!(
      order_status_id: 1,
      order: self
    )
  end

  def bump_order_status
    current_state = self.status.order_status_id
    OrderOrderStatus.create!(
      order_status_id: current_state + 1,
      order: self
    )
  end

  def finalize
    self[:sub_total] = sub_total
    self[:fee_total] = fee_total
    self[:total] = sub_total + fee_total
  end
end

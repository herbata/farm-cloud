class Token < ApplicationRecord
  belongs_to :owner, polymorphic: true

  validates :token_type, presence: true
  validates :token,
    presence: true,
    uniqueness: true


  def self.generate_user_token(type, user)
    token = Token.create({
      owner_type: 'User',
      owner_id: user.id,
      token_type: type,
      token: Token.generate_token([ user.email, user.id ])
    })

    return token.id
  end

  def self.generate_token(params)
    params << Time.now
    Digest::SHA512.hexdigest( params.to_s )
  end
end

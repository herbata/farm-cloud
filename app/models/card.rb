class Card < ApplicationRecord
  belongs_to :customer

  validates :stripe_id, presence: true
  validates :stripe_customer_id, presence: true
  validates :brand, presence: true
  validates :last_four, presence: true
  validates :name, presence: true
  validates :exp_month, presence: true
  validates :exp_year, presence: true
end

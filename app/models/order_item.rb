class OrderItem < ApplicationRecord
  belongs_to :order

  has_many :order_item_product_properties,
    dependent: :destroy
  has_many :product_properties,
    through: :order_item_product_properties

  has_one :order_item_product,
    dependent: :destroy
  has_one :product,
    through: :order_item_product

  validates :sub_total, presence: true
  validates :fee_total, presence: true
  validates :total,     presence: true

  def finalize
    self[:sub_total] = self[:quantity] * product.price
    self[:total] = self[:sub_total] + self[:fee_total]
    self.save!
  end

end

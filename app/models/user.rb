class User < ApplicationRecord
  rolify
  # after_create :create_customer
  after_create :assign_default_role  

  has_many :customers, through: :customer_users
  has_many :customer_users
  
  has_many :tokens,
    dependent:  :destroy,
    as:         :owner,
    inverse_of: :owner

  has_many :phone_numbers,
    dependent:  :destroy,
    as:         :owner,
    inverse_of: :owner


  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
         #:confirmable

  # validates :first_name, presence: true
  # validates :last_name,  presence: true

  def name
    return self.first_name + " " + self.last_name
  end

  def find_or_create_refresh_token
    if !self.active_refresh_token
      self.generate_refresh_token
    end

    return self.refresh_token
  end

  def generate_refresh_token
    token_id = Token.generate_user_token( "refresh", self )
    self.active_refresh_token = token_id
    self.save
  end

  def refresh_token
    return Token.find( self.active_refresh_token )
  end

  def create_customer
    self.customers.create!(name: self.name)
  end

  def link_customer(customer)
    customer.user = self
    customer.save!
  end

  def assign_default_role
    self.add_role(:user) if self.roles.blank?
  end
end

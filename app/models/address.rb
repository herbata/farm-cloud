class Address < ApplicationRecord
  has_one :address_customer
  has_one :customer, through: :address_customer

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :line_one, presence: true
  validates :city, presence: true
  validates :province, presence: true
  validates :country, presence: true
  validates :postal_code, presence: true

  def formatted
    self.line_one.to_s + ", " + (self.line_two ? self.line_two.to_s + ", " : nil.to_s) +  (self.line_three ? self.line_three.to_s + ", " : nil.to_s) + self.city.to_s + ", " + self.province.to_s + ", " + self.country.to_s + ", " + self.postal_code.to_s
  end
end
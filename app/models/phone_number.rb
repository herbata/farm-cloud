class PhoneNumber < ApplicationRecord
  belongs_to :owner, polymorphic: true

  validates :name, presence: true
  validates :value,
    presence:   true,
    uniqueness: true,
    length: {
      minimum:  10,
      too_short: "%{count} characters is the minimum required"
    }

end

class OrderStatus < ApplicationRecord
  has_many :orders, through: :order_order_statuses
  has_many :order_order_statuses

  validates :name,        presence: true
  validates :description, presence: true
end

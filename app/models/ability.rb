class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new

    customer_ids = user.customers.collect { |c| c.valid? ? c.id : nil }

    if user.has_role?(:admin)
      can :manage, :all
    else
      can :read, Product, :active => true
      can :edit, User, User do |u|
        u === user
      end

      can :create, Order
      can :read, Order, Order do |o|
        customer_ids.include?(o.customer.id) if o.customer
      end
      can :manage, OrderItem
      can :manage, Subscription, Subscription do |s|
        customer_ids.include?(s.customer.id) if s.customer
      end
      can :create, Address
      can :manage, Address, Address do |a|
        customer_ids.include?(a.customer.id) if a.customer
      end
    end
  end
end

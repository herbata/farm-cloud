class Property < ApplicationRecord
  has_many :product_properties

  validates :name, presence: true
end

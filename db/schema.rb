# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170217052924) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "address_customers", force: :cascade do |t|
    t.integer  "address_id"
    t.integer  "customer_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["address_id"], name: "index_address_customers_on_address_id", using: :btree
    t.index ["customer_id"], name: "index_address_customers_on_customer_id", using: :btree
  end

  create_table "addresses", force: :cascade do |t|
    t.string   "name"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "line_one"
    t.string   "line_two"
    t.string   "line_three"
    t.string   "city"
    t.string   "province"
    t.string   "country"
    t.string   "postal_code"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "bootsy_image_galleries", force: :cascade do |t|
    t.string   "bootsy_resource_type"
    t.integer  "bootsy_resource_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bootsy_images", force: :cascade do |t|
    t.string   "image_file"
    t.integer  "image_gallery_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cards", force: :cascade do |t|
    t.string   "name"
    t.string   "last_four"
    t.string   "stripe_id"
    t.string   "brand"
    t.string   "exp_month"
    t.string   "exp_year"
    t.integer  "customer_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "stripe_customer_id"
    t.index ["customer_id"], name: "index_cards_on_customer_id", using: :btree
  end

  create_table "charge_charge_types", force: :cascade do |t|
    t.integer  "charge_id"
    t.integer  "charge_type_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["charge_id"], name: "index_charge_charge_types_on_charge_id", using: :btree
    t.index ["charge_type_id"], name: "index_charge_charge_types_on_charge_type_id", using: :btree
  end

  create_table "charge_types", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "charges", force: :cascade do |t|
    t.string   "stripe_id"
    t.string   "stripe_customer_id"
    t.string   "stripe_dispute_id"
    t.string   "stripe_order_id"
    t.string   "stripe_invoice_id"
    t.string   "uuid"
    t.integer  "amount"
    t.integer  "amount_refunded"
    t.string   "application"
    t.string   "application_fee"
    t.string   "balance_transaction"
    t.boolean  "captured"
    t.string   "currency"
    t.text     "description"
    t.string   "destination"
    t.string   "failure_code"
    t.text     "failure_message"
    t.boolean  "livemode"
    t.string   "outcome_type"
    t.string   "outcome_network_status"
    t.string   "outcome_reason"
    t.string   "outcome_risk_level"
    t.string   "outcome_rule_action"
    t.string   "outcome_rule_predicate"
    t.text     "outcome_seller_message"
    t.boolean  "paid"
    t.string   "receipt_email"
    t.string   "receipt_number"
    t.boolean  "refunded"
    t.string   "source"
    t.string   "status"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["uuid"], name: "index_charges_on_uuid", unique: true, using: :btree
  end

  create_table "coupons", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.string   "duration"
    t.integer  "duration_in_months"
    t.integer  "amount_off"
    t.integer  "percent_off"
    t.string   "currency"
    t.integer  "max_redemptions"
    t.datetime "reedem_by"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "customer_users", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "customer_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["customer_id"], name: "index_customer_users_on_customer_id", using: :btree
    t.index ["user_id"], name: "index_customer_users_on_user_id", using: :btree
  end

  create_table "customers", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "primary_address_id"
    t.string   "stripe_id"
  end

  create_table "discounts", force: :cascade do |t|
    t.integer  "coupon_id"
    t.integer  "customer_id"
    t.integer  "subscription_id"
    t.datetime "start"
    t.datetime "end"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["coupon_id"], name: "index_discounts_on_coupon_id", using: :btree
    t.index ["customer_id"], name: "index_discounts_on_customer_id", using: :btree
    t.index ["subscription_id"], name: "index_discounts_on_subscription_id", using: :btree
  end

  create_table "locations", force: :cascade do |t|
    t.string   "name"
    t.string   "country"
    t.string   "province"
    t.string   "city"
    t.string   "address_line_one"
    t.string   "address_line_two"
    t.string   "zip_code"
    t.string   "formatted_address"
    t.float    "lng"
    t.float    "lat"
    t.string   "owner_type"
    t.integer  "owner_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["owner_type", "owner_id"], name: "index_locations_on_owner_type_and_owner_id", using: :btree
  end

  create_table "order_item_product_properties", force: :cascade do |t|
    t.integer  "order_item_id"
    t.integer  "product_property_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.index ["order_item_id"], name: "index_order_item_product_properties_on_order_item_id", using: :btree
    t.index ["product_property_id"], name: "index_order_item_product_properties_on_product_property_id", using: :btree
  end

  create_table "order_item_products", force: :cascade do |t|
    t.integer  "order_item_id"
    t.integer  "product_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["order_item_id"], name: "index_order_item_products_on_order_item_id", using: :btree
    t.index ["product_id"], name: "index_order_item_products_on_product_id", using: :btree
  end

  create_table "order_items", force: :cascade do |t|
    t.float    "sub_total"
    t.float    "fee_total"
    t.float    "total"
    t.integer  "quantity"
    t.text     "notes"
    t.integer  "order_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["order_id"], name: "index_order_items_on_order_id", using: :btree
  end

  create_table "order_order_statuses", force: :cascade do |t|
    t.integer  "order_id"
    t.integer  "order_status_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["order_id"], name: "index_order_order_statuses_on_order_id", using: :btree
    t.index ["order_status_id"], name: "index_order_order_statuses_on_order_status_id", using: :btree
  end

  create_table "order_statuses", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "orders", force: :cascade do |t|
    t.float    "sub_total"
    t.float    "fee_total"
    t.float    "total"
    t.text     "notes"
    t.string   "currency_code"
    t.integer  "customer_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "shipping_address_id"
    t.integer  "billing_address_id"
    t.index ["customer_id"], name: "index_orders_on_customer_id", using: :btree
  end

  create_table "phone_numbers", force: :cascade do |t|
    t.string   "name"
    t.string   "value"
    t.string   "owner_type"
    t.integer  "owner_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["owner_type", "owner_id"], name: "index_phone_numbers_on_owner_type_and_owner_id", using: :btree
  end

  create_table "posts", force: :cascade do |t|
    t.string   "title"
    t.boolean  "published"
    t.integer  "user_id"
    t.string   "meta_image"
    t.string   "cover_image"
    t.text     "content"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["user_id"], name: "index_posts_on_user_id", using: :btree
  end

  create_table "product_groups", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "product_product_types", force: :cascade do |t|
    t.integer  "product_id"
    t.integer  "product_type_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["product_id"], name: "index_product_product_types_on_product_id", using: :btree
    t.index ["product_type_id"], name: "index_product_product_types_on_product_type_id", using: :btree
  end

  create_table "product_properties", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.float    "price"
    t.boolean  "active"
    t.integer  "quantity"
    t.integer  "product_id"
    t.integer  "property_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["product_id"], name: "index_product_properties_on_product_id", using: :btree
    t.index ["property_id"], name: "index_product_properties_on_property_id", using: :btree
  end

  create_table "product_types", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "products", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.boolean  "active"
    t.integer  "price"
    t.string   "currency"
    t.string   "stripe_id"
    t.string   "interval"
    t.integer  "interval_count"
    t.string   "statement_descriptor"
    t.integer  "trial_period_days"
    t.integer  "product_group_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.index ["product_group_id"], name: "index_products_on_product_group_id", using: :btree
  end

  create_table "properties", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.boolean  "allow_multiple"
    t.boolean  "required"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.string   "resource_type"
    t.integer  "resource_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
    t.index ["name"], name: "index_roles_on_name", using: :btree
  end

  create_table "subscriptions", force: :cascade do |t|
    t.integer  "customer_id"
    t.integer  "product_id"
    t.integer  "coupon_id"
    t.string   "uuid"
    t.string   "stripe_id"
    t.string   "stripe_customer_id"
    t.string   "stripe_coupon_id"
    t.string   "stripe_plan_id"
    t.integer  "quantity"
    t.float    "tax_percent"
    t.string   "trial_end"
    t.datetime "canceled_at"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["coupon_id"], name: "index_subscriptions_on_coupon_id", using: :btree
    t.index ["customer_id"], name: "index_subscriptions_on_customer_id", using: :btree
    t.index ["product_id"], name: "index_subscriptions_on_product_id", using: :btree
  end

  create_table "tokens", force: :cascade do |t|
    t.string   "token"
    t.string   "token_type"
    t.string   "owner_type"
    t.integer  "owner_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["owner_type", "owner_id"], name: "index_tokens_on_owner_type_and_owner_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "avatar_url"
    t.integer  "active_refresh_token"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["active_refresh_token"], name: "index_users_on_active_refresh_token", unique: true, using: :btree
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree
  end

  add_foreign_key "address_customers", "addresses"
  add_foreign_key "address_customers", "customers"
  add_foreign_key "cards", "customers"
  add_foreign_key "charge_charge_types", "charge_types"
  add_foreign_key "charge_charge_types", "charges"
  add_foreign_key "customer_users", "customers"
  add_foreign_key "customer_users", "users"
  add_foreign_key "discounts", "coupons"
  add_foreign_key "discounts", "customers"
  add_foreign_key "discounts", "subscriptions"
  add_foreign_key "order_item_product_properties", "order_items"
  add_foreign_key "order_item_product_properties", "product_properties"
  add_foreign_key "order_item_products", "order_items"
  add_foreign_key "order_item_products", "products"
  add_foreign_key "order_items", "orders"
  add_foreign_key "order_order_statuses", "order_statuses"
  add_foreign_key "order_order_statuses", "orders"
  add_foreign_key "orders", "customers"
  add_foreign_key "posts", "users"
  add_foreign_key "product_product_types", "product_types"
  add_foreign_key "product_product_types", "products"
  add_foreign_key "product_properties", "products"
  add_foreign_key "product_properties", "properties"
  add_foreign_key "products", "product_groups"
  add_foreign_key "subscriptions", "coupons"
  add_foreign_key "subscriptions", "customers"
  add_foreign_key "subscriptions", "products"
end

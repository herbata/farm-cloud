#####################
# Product Types
#####################

  ## Product Types

    puts "####################################"
    puts "# Creating Product Types"
    puts "#"

    product_types = [
      {
        id: 1,
        name: 'One-Time Product',
        description: "A purchase made one time."
      }, {
        id: 2,
        name: "Subscription",
        description: "Recurring purchase based on a defined interval."
      }
    ];

    product_types.each do |pt|
      pt_created = ProductType.create!({
        name: pt[:name],
        description: pt[:description]
      })
      puts "# created #{pt_created.name}"
    end

    puts "####################################"


    puts " "
    puts " "
    puts " "


  ## OrderStatuses


    puts "####################################"
    puts "# Creating Order Statuses"
    puts "#"


    order_statuses = [
      {
        id: 1,
        name: "In Progress",
        description: "The order is being created by the Customer."
      }, {
        id: 2,
        name: "Placed",
        description: "The order has been created in The Farm."
      }, {
        id: 3,
        name: "Awaiting Fulfilment",
        description: "The order is awaiting fulfilment by Herbata."
      }, {
        id: 4,
        name: "Fulfilled",
        description: "Order is fulfilled."
      }, {
        id: 5,
        name: "Shipped",
        description: "The order has been shipped!"
      }, {
        id: 6,
        name: "Successfully Delivered",
        description: "The order has been delivered successfully."
      }
    ];

    order_statuses.each do |os|
      os_created = OrderStatus.create!({
        name: os[:name],
        description: os[:description]
      })

      puts "# created #{ os_created.name }"
    end

    puts "####################################"
    puts " "
    puts " "
    puts " "


    ProductGroup.create({
      name: "Basr Products",
      description: "These are the primary products they are featured around the site."
    })


    puts "####################################"
    puts "# Creating Base Products"
    puts "#"

    products = [
      {
        name: "Monthly Subscription",
        stripe_id: "basic-monthly-canada",
        description: "Begin an expedition into the world of tea unlike anything you’ve ever experienced before. This is a monthly recurring subscription that you can cancel anytime.",
        active: true,
        price: "2000",
        currency: "CAD",
        interval: "month",
        interval_count: 1,
        trial_period_days: 0,
        product_group_id: 1,
        product_type_id: 2,
      }, {
        name: "3 Month Gift",
        stripe_id: "basic-three-month-canada",
        description: "The perfect gift for anyone you love: a 3 Month subscription to Herbata.",
        active: true,
        price: "6000",
        currency: "CAD",
        interval: nil,
        interval_count: nil,
        trial_period_days: nil,
        product_group_id: 1,
        product_type_id: 1,
      }, {
        name: "Monthly Subscription",
        stripe_id: "basic-monthly-us",
        description: "Begin an expedition into the world of tea unlike anything you’ve ever experienced before. This is a monthly recurring subscription that you can cancel anytime.",
        active: true,
        price: "2000",
        currency: "USD",
        interval: "month",
        interval_count: 1,
        trial_period_days: 0,
        product_group_id: 1,
        product_type_id: 2,
      }, {
        name: "3 Month Gift",
        stripe_id: "basic-three-month-us",
        description: "The perfect gift for anyone you love: a 3 Month subscription to Herbata.",
        active: true,
        price: "6000",
        currency: "USD",
        interval: nil,
        interval_count: nil,
        trial_period_days: nil,
        product_group_id: 1,
        product_type_id: 1,
      }
    ]

    products.each do |p|
      product = Product.create!({
        name: p[:name],
        description: p[:description],
        active: p[:active],
        price: p[:price],
        stripe_id: p[:stripe_id],
        currency: p[:currency],
        interval: p[:interval],
        interval_count: p[:interval_count],
        trial_period_days: p[:trial_period_days],
        product_group_id: p[:product_group_id],
      })

      product.product_type = ProductType.find(p[:product_type_id])
      product.save!
      
      puts "# created #{ p[:name] }" 
    end

    puts "####################################"
    puts " "
    puts " "
    puts " "

    puts "####################################"
    puts "# Creating Charge Types"
    puts "#"

    charge_types = [
      {
        name: "Charge",
        description: "Charge to a customer's payment source."
      }, {
        name: "Refund",
        description: "Refund to a customer's payment source."
      }
    ]

    charge_types.each do |ct|
      ChargeType.create!({
        name: ct[:name],
        description: ct[:description]
      })

      puts "# created #{ ct[:name] }" 
    end

    puts "####################################"
    puts " "
    puts " "
    puts " "


#####################
# Finsihed
#####################

  puts "#### FINISHED ####"

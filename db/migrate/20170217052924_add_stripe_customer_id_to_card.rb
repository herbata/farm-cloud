class AddStripeCustomerIdToCard < ActiveRecord::Migration[5.0]
  def change
    add_column :cards, :stripe_customer_id, :string
  end
end

class CreateCoupons < ActiveRecord::Migration[5.0]
  def change
    create_table :coupons do |t|
      t.string :name
      t.string :code
      t.string :duration
      t.integer :duration_in_months
      t.integer :amount_off
      t.integer :percent_off
      t.string :currency
      t.integer :max_redemptions
      t.datetime :reedem_by

      t.timestamps
    end
  end
end

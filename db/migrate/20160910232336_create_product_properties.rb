class CreateProductProperties < ActiveRecord::Migration[5.0]
  def change
    create_table :product_properties do |t|
      t.string :name
      t.text :description
      t.float :price
      t.boolean :active
      t.integer :quantity
      t.references :product, foreign_key: true
      t.references :property, foreign_key: true

      t.timestamps
    end
  end
end

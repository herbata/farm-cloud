class CreateCharges < ActiveRecord::Migration[5.0]
  def change
    create_table :charges do |t|
      t.string :stripe_id
      t.string :stripe_customer_id
      t.string :stripe_dispute_id
      t.string :stripe_order_id
      t.string :stripe_invoice_id      
      t.string :uuid
      t.integer :amount
      t.integer :amount_refunded
      t.string :application
      t.string :application_fee
      t.string :balance_transaction
      t.boolean :captured
      t.string :currency
      t.text :description
      t.string :destination
      t.string :failure_code
      t.text :failure_message
      t.boolean :livemode
      t.string :outcome_type
      t.string :outcome_network_status
      t.text :outcome_reason
      t.string :outcome_risk_level
      t.string :outcome_rule_action
      t.string :outcome_rule_predicate
      t.text :outcome_seller_message
      t.string :outcome_reason
      t.boolean :paid
      t.string :receipt_email
      t.string :receipt_number
      t.boolean :refunded
      t.string :source
      t.string :status

      t.timestamps
    end
    add_index :charges, :uuid, unique: true
  end
end

class CreateAddressCustomers < ActiveRecord::Migration[5.0]
  def change
    create_table :address_customers do |t|
      t.references :address, foreign_key: true
      t.references :customer, foreign_key: true

      t.timestamps
    end
  end
end

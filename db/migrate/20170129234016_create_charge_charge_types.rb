class CreateChargeChargeTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :charge_charge_types do |t|
      t.references :charge, foreign_key: true
      t.references :charge_type, foreign_key: true

      t.timestamps
    end
  end
end

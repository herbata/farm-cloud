class CreatePhoneNumbers < ActiveRecord::Migration[5.0]
  def change
    create_table :phone_numbers do |t|
      t.string :name
      t.string :value

      t.references :owner, index: true, polymorphic: true

      t.timestamps
    end
  end
end

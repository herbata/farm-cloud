class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :name
      t.text :description
      t.boolean :active
      t.integer :price
      t.string :currency
      t.string :stripe_id
      t.string :interval
      t.integer :interval_count
      t.string :statement_descriptor
      t.integer :trial_period_days
      t.references :product_group, foreign_key: true

      t.timestamps
    end
  end
end

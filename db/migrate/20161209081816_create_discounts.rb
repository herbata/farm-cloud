class CreateDiscounts < ActiveRecord::Migration[5.0]
  def change
    create_table :discounts do |t|
      t.references :coupon, foreign_key: true
      t.references :customer, foreign_key: true
      t.references :subscription, foreign_key: true
      t.datetime :start
      t.datetime :end

      t.timestamps
    end
  end
end

class CreateOrderItemProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :order_item_products do |t|
      t.references :order_item, foreign_key: true
      t.references :product, foreign_key: true

      t.timestamps
    end
  end
end

class CreateOrderOrderStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :order_order_statuses do |t|
      t.references :order, foreign_key: true
      t.references :order_status, foreign_key: true

      t.timestamps
    end
  end
end

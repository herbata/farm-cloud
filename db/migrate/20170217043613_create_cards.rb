class CreateCards < ActiveRecord::Migration[5.0]
  def change
    create_table :cards do |t|
      t.string :name
      t.string :last_four
      t.string :stripe_id
      t.string :brand
      t.string :exp_month
      t.string :exp_year
      t.references :customer, foreign_key: true

      t.timestamps
    end
  end
end

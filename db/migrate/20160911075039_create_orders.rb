class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.float :sub_total
      t.float :fee_total
      t.float :total
      t.text :notes
      t.string :currency_code
      t.references :customer, foreign_key: true

      t.timestamps
    end
  end
end

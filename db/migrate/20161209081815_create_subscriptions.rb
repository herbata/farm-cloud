class CreateSubscriptions < ActiveRecord::Migration[5.0]
  def change
    create_table :subscriptions do |t|
      t.references :customer, foreign_key: true
      t.references :product, foreign_key: true
      t.references :coupon, foreign_key: true
      t.string :uuid
      t.string :stripe_id
      t.string :stripe_customer_id
      t.string :stripe_coupon_id
      t.string :stripe_plan_id
      t.integer :quantity
      t.float :tax_percent
      t.string :trial_end
      t.datetime :canceled_at

      t.timestamps
    end
  end
end

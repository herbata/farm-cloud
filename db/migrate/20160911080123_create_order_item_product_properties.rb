class CreateOrderItemProductProperties < ActiveRecord::Migration[5.0]
  def change
    create_table :order_item_product_properties do |t|
      t.references :order_item, foreign_key: true
      t.references :product_property, foreign_key: true

      t.timestamps
    end
  end
end

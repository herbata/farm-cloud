Rails.application.routes.draw do
  
  ## Frontend Website Routes
  root to: 'frontend#index'
  get 'blog', to: 'frontend#blog', as: :blog
  get 'order', to: 'frontend#order', as: :order
  get 'products', to: 'frontend#products', as: :frontend_products
  get 'about', to: 'frontend#about', as: :about
  get 'contact', to: 'frontend#contact', as: :contact
  get 'cart', to: 'frontend#cart', as: :cart
  get 'checkout/account', to: 'frontend#checkout_account', as: :checkout_account
  get 'checkout/shipping', to: 'frontend#checkout_shipping', as: :checkout_shipping
  get 'checkout/payment', to: 'frontend#checkout_payment', as: :checkout_payment
  get 'checkout/confirmation', to: 'frontend#checkout_confirmation', as: :checkout_confirmation

  resources :order_items, only: [:create, :update, :destroy]
  resources :orders, only: [:update]

  post 'addresses', to: 'addresses#create'
  post 'checkout/payment', to: 'frontend#checkout_payment_fulfillment', as: :checkout_payment_fulfillment

  ## User Account Routes
  get 'account', to: 'account#index', as: :account
  scope 'account' do
    get 'subscriptions', to: 'account#subscriptions', as: :account_subscriptions
    post 'subscriptions/:id/cancel', to: 'subscriptions#cancel', as: :cancel_subscription
    # get 'billing', to: 'account#billing', as: :account_billing
    get 'settings', to: 'account#settings', as: :account_settings
    resources :addresses, except: :destroy
  end


  ## Verify Let's Encrypt
  get '.well-known/acme-challenge/krDZFT9l6mV5mh29OuTAinc3-uCDZudYljSon6gH41I', to: 'frontend#letsencrypt_develop'
  get '.well-known/acme-challenge/bZQzDR1ujn0FQ0UuIJYwrpHINu-Jkpnv4I1XtAvcNhs', to: 'frontend#letsencrypt_staging'
  get '.well-known/acme-challenge/4ahKSK8Se_NIIpBnfipB0xIFNU6mhg69pocJU4GzV8o', to: 'frontend#letsencrypt_production'


  ## 301's from old Website
  get 'blogs/news', to: redirect('http://blog.herbata.co', status: 301)
  get 'blogs/news/:name', to: redirect('http://blog.herbata.co/%{name}', status: 301)
  get 'collections/all', to: redirect('https://herbata.co/products', status: 301)
  get 'products/one-month-subscription', to: redirect('https://herbata.co/products', status: 301)
  get 'products/3-month-subscription', to: redirect('https://herbata.co/products', status: 301)

  

  ## Devise Routes
  devise_for :users, :controllers => {
    registrations: 'registrations',
    sessions: 'sessions',
    passwords: 'passwords'
  }
  devise_scope :user do
    get 'login', to: 'devise/sessions#new'
    delete 'logout', to: 'devise/sessions#destroy'
  end


  ## Backend Access Routes
  get 'farm', to: 'dashboard#index', as: :dashboard
  scope 'farm' do
    resources :orders, only: [:index, :show, :update] do
      resources :order_statuses, only: [:index, :create]
    end
    get 'orders/:id', to: 'orders#show', as: :order_detail

    resources :product_groups
    resources :products
    resources :coupons
    resources :subscriptions
    resources :customers
  end
end
